import { config } from '../wdio.shared.conf';

// ============
// Specs
// ============
config.specs = [
    './tests/specs/**/app*.spec.js',
];
config.exclude = [
    // Exclude this one because the test can only be executed on emulators/simulators
    './tests/specs/**/app.biometric.login.spec.js',
];

// =============================
// Browserstack specific config
// =============================
// User configuration
config.user = process.env.BROWSERSTACK_USER || 'ankur_7QhYwC';
config.key = process.env.BROWSERSTACK_ACCESS_KEY || 'qrZtazCpVFtAVP2MQ8AW';
config.hostname = 'hub.browserstack.com';

// Use browserstack service
config.services = [
    ['browserstack', {
        app: process.env.BROWSERSTACK_APP_ID || 'bs://735d263574973dd36753d8c8ba4fc3e948033cb5',
    }]
];
// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities
config.capabilities = [
    {
        'bstack:options': {
            deviceName: 'Google Pixel 7 Pro',
            // Set your BrowserStack config
            debug: true,
            platformName: 'android',

            // // Specify device and os_version for testing
            osVersion: '13.0',
            local: false,
            // // Set other BrowserStack capabilities
            projectName: 'wdio-test-project',
            buildName: 'android',
            sessionName: 'wdio-test'
        }
    },
];

exports.config = config;